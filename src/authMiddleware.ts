import { createConnection, getRepository } from "typeorm"
import { User } from './entity/User'
const jwt = require('jsonwebtoken')

export const Middleware = async (req, res, next) => {
    try {
        const [, token] = await req.headers.authorization.split(' ');

        const payload = await jwt.verify(token, 'secretkey');
        const userMail = payload.mail;

        const user = await verifyUser(userMail)
        if (!user) {
            return res.status(401);
        }

        req.auth = user;

        next();
    } catch (error) {
        console.log(error)
        res.status(401).send(error);
    }

}

const verifyUser = async (userMail) => {
    return getRepository(User)
        .createQueryBuilder()
        .where("mail = :mail",
            { mail: userMail })
        .getOne();
}

export default Middleware