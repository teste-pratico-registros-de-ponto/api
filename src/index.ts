import "reflect-metadata"
import * as express from "express"
import * as cors from 'cors'
import { graphqlExpress } from 'apollo-server-express'
import { createConnection } from "typeorm"
import * as bodyParser from "body-parser"
import authMiddleware from './authMiddleware'
import checkUserRoot from './configuration/checkUserRoot'
import { schema } from './schemas/schema'
import corsOptions from './configuration/corsOptions'
const routeAuth = require("./routes/auth")
const bcrypt = require('bcrypt')

const options: cors.CorsOptions = corsOptions()

const PORT = 3200

createConnection().then(async connection => {
  await checkUserRoot();

  const app = express();
  app.use(cors(options));
  app.options("*", cors(options));
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use('/auth', routeAuth);
  app.use('/main', authMiddleware, bodyParser.json(),
    graphqlExpress(request =>
      ({
        schema,
        context: { token: request.headers.authorization.split(' ')[1] }
      })));

  app.listen(PORT);

  console.log(`Rodando na porta ${PORT}`);

}).catch(error => console.log(error));
