import { makeExecutableSchema } from 'graphql-tools'
import { gql } from 'apollo-server'

import users from './resolvers/query/users'
import user from './resolvers/query/user'
import historyTimeUser from './resolvers/query/historyTimeUser'
import adminViewHistories from './resolvers/query/adminViewHistories'

import historyTimeUserMutation from './resolvers/mutations/historyTimeUser'
import departureTime from './resolvers/mutations/departureTime'
import userMutation from './resolvers/mutations/user'

const moment = require('moment');

const typeDefs = gql`
type User {
  id: Int,
  name: String,
  mail: String,
  admin: Boolean,
}

type RegisteredTime {
  id: Int,
  entryTime: String,
  departureTime: String,
  user: User
}

type Query {
  users: [User]
  user: User
  historyTimeUser: [RegisteredTime]
  adminViewHistories: [RegisteredTime]
}

type Mutation {
  historyTimeUser: RegisteredTime
  departureTime(id: Int): RegisteredTime
  user(name: String!, mail: String!, password: String!): User
}

`;

const resolvers = {
  Query: {
    users: () => users(),
    user: (root, args, context) => user(root, args, context),
    historyTimeUser: (root, args, context) => historyTimeUser(root, args, context),
    adminViewHistories: (root, args, context) => adminViewHistories(root, args, context),
  },
  Mutation: {
    historyTimeUser: (parent, args, context, info) => historyTimeUserMutation(parent, args, context, info),
    departureTime: (parent, args, context, info) => departureTime(parent, args, context, info),
    user: (parent, args, context, info) => userMutation(parent, args, context, info),
  }
};

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});
