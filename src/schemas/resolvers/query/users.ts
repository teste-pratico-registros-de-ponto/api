import { getRepository, getConnection, Connection } from "typeorm"
import { User } from '../../../entity/User'

export const users = () => {
    return getRepository(User).createQueryBuilder().getMany()
}

export default users