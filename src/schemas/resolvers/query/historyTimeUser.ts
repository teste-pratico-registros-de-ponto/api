import { getRepository, getConnection, Connection } from "typeorm"
import { User } from '../../../entity/User'
import { RegisteredTime } from '../../../entity/RegisteredTime'
const jwt = require('jsonwebtoken')

export const historyTimeUser = async (root, args, context) => {
    const token = context.token
    const payload = await jwt.verify(token, 'secretkey')
    const mail = payload.mail
    const user = await getRepository(User).createQueryBuilder("user").where("mail = :mail", { mail }).getOne()
    return await
        getRepository(RegisteredTime).createQueryBuilder("reg")
            .select(["reg", "user.name"])
            .where("UserId = :id", { id: user["id"] })
            .leftJoin("reg.user", "user")
            .orderBy("reg.id", "DESC")
            .limit(10)
            .getMany()
}

export default historyTimeUser