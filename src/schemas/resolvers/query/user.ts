import { getRepository, getConnection, Connection } from "typeorm"
import { User } from '../../../entity/User'
const jwt = require('jsonwebtoken')

export const user = async (root, args, context) => {
    const token = context.token
    const payload = await jwt.verify(token, 'secretkey')
    const mail = payload.mail
    return getRepository(User).createQueryBuilder().where("mail = :mail", { mail }).getOne()
}

export default user