import { getRepository, getConnection, Connection } from "typeorm"
import { AuthenticationError } from 'apollo-server'
import { RegisteredTime } from '../../../entity/RegisteredTime'
const jwt = require('jsonwebtoken')

export const adminViewHistories = async (root, args, context) => {
    const token = context.token
    const payload = await jwt.verify(token, 'secretkey')
    const admin = Boolean(payload.admin)

    if (!admin) throw new AuthenticationError('Precisa de autenticação.')

    return await
        getRepository(RegisteredTime).createQueryBuilder("reg")
            .select(["reg", "user.name"])
            .leftJoin("reg.user", "user")
            .orderBy("reg.id", "DESC")
            .limit(10)
            .getMany()
}

export default adminViewHistories