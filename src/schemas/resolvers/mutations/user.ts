import { getRepository, getConnection, Connection } from "typeorm"
import { AuthenticationError } from 'apollo-server'
import { RegisteredTime } from '../../../entity/RegisteredTime'
import { User } from '../../../entity/User'
const jwt = require('jsonwebtoken')
const moment = require('moment');
const bcrypt = require('bcrypt')

export const user = async (parent, args, context, info) => {
    const token = context.token
    const payload = await jwt.verify(token, 'secretkey')
    const admin = Boolean(payload.admin)

    if (!admin) throw new AuthenticationError('Precisa de autenticação.')

    const name = args.name
    const mail = args.mail
    let password = args.password

    await bcrypt.hash(password, 15, async (err, hash) => {
        if (err) throw new AuthenticationError('Erro de criação')

        return getConnection().createQueryBuilder().insert()
            .into(User)
            .values({
                name,
                mail,
                password: hash,
                admin: false
            })
            .execute()
    })
}

export default user