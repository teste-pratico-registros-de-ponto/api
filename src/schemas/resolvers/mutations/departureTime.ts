import { getRepository, getConnection, Connection } from "typeorm"
import { AuthenticationError } from 'apollo-server'
import { RegisteredTime } from '../../../entity/RegisteredTime'
import { User } from '../../../entity/User'
const jwt = require('jsonwebtoken')
const moment = require('moment');

export const departureTime = async (parent, args, context, info) => {
    const id = args.id
    const date = await moment().format()
    return await getConnection()
        .createQueryBuilder()
        .update(RegisteredTime)
        .set({ departureTime: date })
        .where("id = :id", { id })
        .execute();
}

export default departureTime