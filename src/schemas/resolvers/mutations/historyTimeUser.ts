import { getRepository, getConnection, Connection } from "typeorm"
import { AuthenticationError } from 'apollo-server'
import { RegisteredTime } from '../../../entity/RegisteredTime'
import { User } from '../../../entity/User'
const jwt = require('jsonwebtoken')
const moment = require('moment');

export const historyTimeUser = async (parent, args, context, info) => {
    const token = context.token
    const payload = await jwt.verify(token, 'secretkey')
    const mail = payload.mail

    const user = await getRepository(User).createQueryBuilder("user").where("mail = :mail", { mail: mail }).getOne()
    const date = moment().format()
    return getConnection().createQueryBuilder().insert()
        .into(RegisteredTime)
        .values({
            entryTime: date,
            user: user
        })
        .execute()
}

export default historyTimeUser