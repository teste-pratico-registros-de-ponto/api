const express = require('express')
const router = express.Router()
const bodyParser = require("body-parser")
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

import { getRepository, getConnection, Connection } from "typeorm";
import { User } from "../entity/User";

router.post('/login', async (request, response) => {
    const mail = request.body.mail
    const password = request.body.password

    try {
        let userDB = await getRepository(User)
            .createQueryBuilder()
            .where("mail = :mail",
                { mail })
            .getOne();

        bcrypt.compare(request.body.password, userDB['password'], async (err, result) => {
            if (err) {
                response.status(401).send("Erro de autenticação");
            }
            if (result) {
                await jwt.sign({ mail: mail, admin: Boolean(userDB['admin']) }, 'secretkey', { expiresIn: '60h' }, (err, token) => {
                    if (err) { response.status(401).send("Erro de autenticação") }
                    response.json({
                        token,
                        mail,
                    });
                });
            } else {
                response.status(401).send("Erro de autenticação");
            }
        })
    } catch (err) {
        response.status(401).send(err);
    }
});

router.post('/register', async (request, response) => {
    const name = request.body.name;
    const mail = request.body.mail;
    const password = request.body.password;

    if (!name || !mail || !password) {
        response.send("Dados incompletos");
    }

    try {
        try {
            const nameAlreadyExist = await getRepository(User)
                .createQueryBuilder()
                .where("name = :name", { name: name })
                .getOne();

            const mailAlreadyExist = await getRepository(User)
                .createQueryBuilder()
                .where("mail = :mail", { mail: mail })
                .getOne();

            if (nameAlreadyExist) response.status(400).send({ error: "Nome de usuário já existe." });
            if (mailAlreadyExist) response.status(400).send({ error: "Este email já está cadastrado." });
        } catch (e) {
            response.send(e);
        }

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(User)
            .values([
                { name: name, mail: mail, password: password, admin: false }
            ])
            .execute();
        response.send("Usuário Criado.");
    } catch (e) {
        response.send(e);
    }
});

router.get('/teste', async (request, response) => {
    const mail = "a"
    const id = await getRepository(User).createQueryBuilder("user").where("mail = :mail", { mail: "a" }).select(["user.id"]).getOne()
    console.log(id["id"])
    response.send("id: " + id)
});

module.exports = router;