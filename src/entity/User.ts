import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { RegisteredTime } from "./RegisteredTime"

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 40 })
    name: string;

    @Column("varchar", { length: 60 })
    mail: string;

    @Column("varchar", { length: 255 })
    password: string;

    @Column("bool")
    admin: boolean;

    @OneToMany(type => RegisteredTime, registeredTime => registeredTime.user)
    registeredTime: RegisteredTime[];

}