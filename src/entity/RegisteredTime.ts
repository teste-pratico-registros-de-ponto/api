import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from './User'

@Entity()
export class RegisteredTime {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("datetime")
    entryTime: string;

    @Column("datetime", { nullable: true })
    departureTime: string;

    @ManyToOne(type => User, user => user.registeredTime)
    user: User;

}