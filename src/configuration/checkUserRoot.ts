import { User } from '../entity/User'
import { createConnection, getRepository } from "typeorm"
const bcrypt = require('bcrypt')

export default async () => {
    const userRootCheck = await getRepository(User).createQueryBuilder().where("mail = :mail", { mail: "root" }).getOne()
    if (!userRootCheck) {
        const name = "Administrador root"
        const mail = "root"
        const password = "123"

        await bcrypt.hash(password, 15, async (err, hash) => {
            return getRepository(User).createQueryBuilder().insert()
                .values({
                    name,
                    mail,
                    password: hash,
                    admin: true
                })
                .execute()
        })
    }
}